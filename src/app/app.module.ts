import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { HttpClientModule } from "@angular/common/http";

import { LoaderComponent } from './components/loader/loader.component';
import { SidenavLeftComponent } from './components/sidenav-left/sidenav-left.component';
import { SidenavLeftService } from "./components/sidenav-left/sidenav-left.service";

import {TrendModule} from "./components/trend/trend.module";

import { FeedComponent } from './pages/feed/feed.component';
import { TrendComponent } from './pages/trend/trend.component';

@NgModule({
  declarations: [
    AppComponent,
    LoaderComponent,
    SidenavLeftComponent,
    FeedComponent,
    TrendComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    TrendModule
  ],
  providers: [SidenavLeftService],
  bootstrap: [AppComponent]
})
export class AppModule { }
