import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {FeedComponent} from "./pages/feed/feed.component";
import {TrendComponent} from "./pages/trend/trend.component";

const routes: Routes = [
  {
    path: '',
    redirectTo: 'feed',
    pathMatch: 'full'
  },
  { path: 'feed', component: FeedComponent },
  { path: 'feed/:provider', component: FeedComponent },
  { path: 'trend/:id', component: TrendComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
