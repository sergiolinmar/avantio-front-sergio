export class Trend {
    _id: string;
    title: string = '';
    body: string = '';
    provider: string = '';
    image: string = '';
    url: string = '';
    createdAt: Date;

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}