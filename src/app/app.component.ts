import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <sidenav-left></sidenav-left>
    <div class="today-box"><span>{{today.getDate()}} {{months[today.getMonth()]}} {{today.getFullYear()}}</span></div>
    <router-outlet></router-outlet>
  `,
  styles: []
})
export class AppComponent {
  today = new Date();
  months = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
}
