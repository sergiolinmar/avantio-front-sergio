import { Component, OnInit } from '@angular/core';
import {SidenavLeftService} from "../../components/sidenav-left/sidenav-left.service";
import {TrendService} from "../../services/trend.service";
import {Trend} from "../../models/trend";
import {ActivatedRoute} from "@angular/router";

@Component({
    selector: 'app-feed',
    templateUrl: './feed.component.html',
    styleUrls: ['./feed.component.scss']
})
export class FeedComponent implements OnInit {
    trends: Trend[];
    provider: string; // provider from URL for filter
    loading:boolean = false;

    constructor(private sidenavLeft: SidenavLeftService, private trendService: TrendService, private route: ActivatedRoute) {
        sidenavLeft.mini = false;

        // Control provider from url
        route.params.subscribe( (params) => {
            if(params.provider){
                this.provider = params.provider;
            }

            this.getFeed();
        });
    }

    ngOnInit() {

    }

    getFeed() {
        this.loading = true;
        this.trendService.getAll()
            .subscribe(
                (trends) => {
                    this.sidenavLeft.trendsCounter = trends.length;

                    // Filter by provider
                    if(this.provider) {
                        this.trends = trends.filter((trend) => (trend.provider == this.provider) );
                    } else {
                        this.trends = trends;
                    }

                    this.loading = false;
                }
            );
    }

    /* Get the two trends key 1 and 2 */
    filterMediumTrends() {
        if(!this.trends) return;
        return this.trends.filter((trend, index) => (index > 0 && index <= 2) )
    }

    /* Get the rest of trends key > 2 */
    filterSmallTrends() {
        if(!this.trends) return;
        return this.trends.filter((trend, index) => (index >= 3) )
    }

}
