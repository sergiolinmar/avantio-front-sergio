import { Component, OnInit } from '@angular/core';
import {SidenavLeftService} from "../../components/sidenav-left/sidenav-left.service";
import {TrendService} from "../../services/trend.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Trend} from "../../models/trend";

@Component({
    selector: 'app-trend',
    templateUrl: './trend.component.html',
    styleUrls: ['./trend.component.scss']
})
export class TrendComponent implements OnInit {
    trend: Trend;
    showTrendForm:boolean = false; // show and hide panel form
    trendToEdit: any; // clone of trend for edit
    loading:boolean = false;

    constructor(private sidenavLeft: SidenavLeftService, private trendService: TrendService, private route: ActivatedRoute, private router: Router) {
        sidenavLeft.mini = true;

        // Get trend if from url
        route.params.subscribe( (params) => {
            if(params.id){
                this.getTrend(params.id);
            }
        });
    }

    ngOnInit() {
    }

    getTrend(trendId) {
        this.loading = true;
        this.trendService.getById(trendId)
            .subscribe(
                (trend) => {
                    this.trend = trend;
                    this.loading = false;
                }
            );
    }

    deleteTrend() {
        this.trendService.delete(this.trend._id)
            .subscribe(
                (data) => {
                    this.router.navigate(['/feed']);
                }
            );
    }

    editTrend() {
        this.trendToEdit = this.trend;
        this.showTrendForm = true;
    }

}
