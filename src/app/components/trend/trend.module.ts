import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TrendPreviewComponent} from "./trend-preview/trend-preview.component";
import {TrendFormComponent} from "./trend-form/trend-form.component";
import {ReactiveFormsModule} from "@angular/forms";
import {AppRoutingModule} from "../../app-routing.module";



@NgModule({
  declarations: [
    TrendPreviewComponent,
    TrendFormComponent
  ],
  exports: [
    TrendPreviewComponent,
    TrendFormComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    ReactiveFormsModule
  ]
})
export class TrendModule { }
