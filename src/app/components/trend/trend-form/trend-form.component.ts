import {Component, ElementRef, EventEmitter, Input, OnInit, OnChanges, Output} from '@angular/core';
import { trigger, style, animate, transition } from '@angular/animations';
import {Trend} from "../../../models/trend";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {TrendService} from "../../../services/trend.service";
import {Router} from "@angular/router";

@Component({
    selector: 'trend-form',
    animations: [
        trigger(
            'slideAnimation', [
                transition(':enter', [
                    style({transform: 'translateX(100%)'}),
                    animate('200ms', style({transform: 'translateX(0)'}))
                ]),
                transition(':leave', [
                    style({transform: 'translateX(0)'}),
                    animate('200ms', style({transform: 'translateX(100%)'}))
                ])
            ]
        ),
        trigger(
            'opacityAnimation', [
                transition(':enter', [
                    style({opacity: 0}),
                    animate('500ms', style({opacity: 1}))
                ]),
                transition(':leave', [
                    style({opacity: 1}),
                    animate('500ms', style({opacity: 0}))
                ])
            ]
        )
    ],
    templateUrl: './trend-form.component.html',
    styleUrls: ['./trend-form.component.scss']
})
export class TrendFormComponent implements OnInit, OnChanges {
    @Input() trend: Trend;
    @Input() showPanel: boolean = false;
    @Output() showPanelChange: EventEmitter<boolean> = new EventEmitter<boolean>();

    trendForm: FormGroup;
    mode:string = 'edit'; // [edit/add]

    constructor(private el: ElementRef, private trendService: TrendService, private router: Router) {

    }

    ngOnInit() {
        this.ngOnChanges();
    }

    ngOnChanges() {
        // Control trend variable passed in html component
        if(this.trend) {
            this.trendForm = new FormGroup({
                url: new FormControl((this.mode == 'edit' ? this.trend.url : ''), Validators.compose([
                    Validators.required
                ])),
                title: new FormControl((this.mode == 'edit' ? this.trend.title : ''), Validators.compose([
                    Validators.required
                ])),
                body: new FormControl((this.mode == 'edit' ? this.trend.body : ''), Validators.compose([
                    Validators.required
                ])),
            });
        }

        // Remove overflow when panel is edit mode
        if(this.showPanel) {
            this.showBodyOverflow(false);
        }
    }

    /* Only call in add button */
    showAddPanel() {
        this.mode = 'add';
        this.trend = new Trend();
        this.ngOnChanges();
        this.showPanel = true;
        this.showPanelChange.emit(true);
        this.showBodyOverflow(false);
    }

    closePanel() {
        this.mode = 'edit';
        this.showPanel = false;
        this.showPanelChange.emit(false);
        this.showBodyOverflow(true);
    }

    saveTrend() {
        if(!this.trend._id) { // Create new trend
            const newTrend = new Trend(this.trendForm.value);
            newTrend.image = 'https://emtstatic.com/2020/02/iStock-922747782.jpg';
            newTrend.provider = 'elpais';

            this.trendService.create(newTrend)
                .subscribe(
                    (trend) => {
                        this.closePanel();
                        this.router.navigate(['/trend', trend._id]);
                    }
                );
        } else { // Edit trend

            // image is required from endpoint
            if(!this.trend.image) {
                this.trend.image = 'https://emtstatic.com/2020/02/iStock-922747782.jpg';
            }

            this.trendService.update(this.trend)
                .subscribe(
                    () => {
                        this.closePanel();
                    }
                );
        }

    }

    // Control the body overflow to prevent scroll
    showBodyOverflow(show) {
        this.el.nativeElement.closest('body').style.overflowY = (show ? 'auto' : 'hidden');
    }
}