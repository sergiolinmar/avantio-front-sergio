import {Component, Input, OnInit} from '@angular/core';
import {Trend} from "../../../models/trend";

@Component({
  selector: 'trend-preview',
  templateUrl: './trend-preview.component.html',
  styleUrls: ['./trend-preview.component.scss']
})
export class TrendPreviewComponent implements OnInit {
  @Input() trend: Trend;
  @Input() mode: string;  // [big, medium, small] type of view of the trend preview

  constructor() { }

  ngOnInit() {
  }

}
