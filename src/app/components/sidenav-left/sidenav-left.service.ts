import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SidenavLeftService {
  mini: boolean; // convert sidenav in minimal width
  trendsCounter: number = 0;

  constructor() { }
}
