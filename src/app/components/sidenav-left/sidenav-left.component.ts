import {Component, OnInit} from '@angular/core';
import {SidenavLeftService} from "./sidenav-left.service";

@Component({
  selector: 'sidenav-left',
  templateUrl: './sidenav-left.component.html',
  styleUrls: ['./sidenav-left.component.scss']
})
export class SidenavLeftComponent implements OnInit {

  constructor(private sidenavLeft: SidenavLeftService) {

  }

  ngOnInit() {
  }

}
