import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import { catchError, map } from 'rxjs/operators';
import {Trend} from "../models/trend";

const API_URL = environment.apiUrl;
const headers = new HttpHeaders()
    .set('X-Avantio-Auth', 'ooozroqnd48wcl63jzs5c');

@Injectable({
    providedIn: 'root'
})
export class TrendService {

    constructor(private http: HttpClient) { }

    public getAll(): Observable<Trend[]> {
        return this.http
            .get(API_URL + '/trends', {headers: headers}).pipe(
                map((data:any) => {
                    return data.trends.map((trend) => new Trend(trend));
                }),
                catchError(TrendService.handleError)
            )
    }

    public getById(trendId: string): Observable<Trend> {
        return this.http
            .get(API_URL + '/trends/' + trendId, {headers: headers}).pipe(
                map((data:any) => {
                    return new Trend(data.trend);
                }),
                catchError(TrendService.handleError)
            );
    }

    public create(trend: Trend): Observable<Trend> {
        return this.http
            .post(API_URL + '/trends' , trend, {headers: headers}).pipe(
                map((data:any) => {
                    return new Trend(data.trend);
                }),
                catchError(TrendService.handleError)
            );
    }

    public update(trend: Trend): Observable<Trend> {
        return this.http
            .put(API_URL + '/trends/' + trend._id, {title: trend.title, body: trend.body, url: trend.url},{headers: headers}).pipe(
                map((data:any) => {
                    return new Trend(data.trend);
                }),
                catchError(TrendService.handleError)
            );
    }

    public delete(trendId: string): Observable<Trend> {
        return this.http
            .delete(API_URL + '/trends/' + trendId, {headers: headers}).pipe(
                map((data:any) => {
                    return data;
                }),
                catchError(TrendService.handleError)
            );
    }

    private static handleError (error: Response | any) {
        console.error('TrendService::handleError', error);
        return throwError(error);
    }

}
