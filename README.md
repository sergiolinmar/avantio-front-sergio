# Prueba front - Sergio Linares

## Prerequisites

- Node.js version 10.9.0 or later.
- NPM package manager.
- Angular CLI: `npm install -g @angular/cli`
- Run: `npm install`

## Run the application

- Run  `ng serve --open`
- Navigate to `http://localhost:4200/`